# products.button-broadcast

[![pdm-managed](https://img.shields.io/badge/pdm-managed-blueviolet)](https://pdm-project.org)

> Micro apps for MQTT pub/sub comms driven by device events, like USB buttons

Have you ever wanted to trigger an MQTT message publish by pressing a button? Now you can! This monorepo includes two tiny applications: [`evdev_mqtt_pub`](./packages/evdev_mqtt_pub) for
handling device events and publishing MQTT messages, and [`pushover_mqtt_sub`](./packages/pushover_mqtt_sub/`) for subscribing to the configured topic and alerting you via
[Pushover][links.pushover]. Also included is [`mqtt_utils`](./packages/mqtt_utils), a small wrapper library for the Paho MQTT client.

This proof-of-concept(s) was put together using the following projects for help and inspiration:

- [_How to Use MQTT in Python with Paho Client_ (emqx.com)][links.emq-example]
- [github.com/janvda/balena-python-evdev2mqtt][links.github.balena-python-evdev2mqtt]
- [github.com/ayavilevich/evdev2hass][links.github.evdev2hass]

## Bring Your Own Broker

This project is not tied to any particular broker; just the Paho Client library. EMQX offers a good managed free tier with their serverless broker product,
or you can host a broker yourself.

## USB Devices and Containers

**Important: make sure to use [Docker CE][links.docker.ce], not [Desktop][links.docker.desktop].** Desktop won't allow you to expose most input
devices to the container using e.g. `docker run --dit --device=/dev/input/js0`. This means the `evdev_mqtt_pub` package
likely won't be able to access the input device you use to trigger the MQTT pubs.

Fortunately, if you're running this code on a [single-board computer (SBC)][links.wiki.sbc], you probably won't be using Docker Desktop, so this won't
be a problem. I encountered it during development on a GNU/Linux desktop machine, and I imagine it would also occur using WSL.

<!-- Links -->

[links.pushover]: https://pushover.net/
[links.emq-example]: https://www.emqx.com/en/blog/how-to-use-mqtt-in-python
[links.github.balena-python-evdev2mqtt]: https://github.com/janvda/balena-python-evdev2mqtt
[links.github.evdev2hass]: https://github.com/ayavilevich/evdev2hass
[links.docker.ce]: https://docs.docker.com/engine/install/
[links.docker.desktop]: https://www.docker.com/products/docker-desktop/
[links.wiki.sbc]: https://en.wikipedia.org/wiki/Single-board_computer
