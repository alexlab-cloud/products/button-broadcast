"""Core MQTT utilities built by wrapping Paho's MQTT library in dataclasses and functions.

Useful for basic MQTT pub/sub clients.
"""

import os
from typing import Self, Any
from dataclasses import dataclass
from pathlib import Path

from paho.mqtt.client import Client as MQTTClient
from paho.mqtt.client import ConnectFlags as MQTTConnectFlags
from paho.mqtt.client import MQTTMessage, MQTTMessageInfo
from paho.mqtt.properties import Properties as MQTTProperties
from paho.mqtt.reasoncodes import ReasonCode as MQTTReasonCode
from paho.mqtt.enums import CallbackAPIVersion as MQTTCallbackAPIVersion

from loguru import logger


@dataclass
class MQTTClientConfig:
    """A simple data object for MQTT client setup."""

    broker: str
    port: int
    client_id: str
    username: str
    password: str

    def __post_init__(self):
        for key, val in self.__dict__.items():
            if val is None:
                raise ValueError(f"{__class__}: {key} cannot be {val}")

    @classmethod
    def from_env(cls: Self) -> Self:
        """Return a MqttClientConfig instance from environment variables prefixed with `MQTT_`."""

        return MQTTClientConfig(
            broker=os.getenv("MQTT_BROKER"),
            port=int(os.getenv("MQTT_PORT")),
            client_id=os.getenv("MQTT_CLIENT_ID"),
            username=os.getenv("MQTT_USERNAME"),
            password=os.getenv("MQTT_PASSWORD"),
        )


@dataclass
class MQTTMessagePayload:
    """Data necessary to send a MQTT message to a configured broker."""

    topic: str
    message: str

    @classmethod
    def from_dict(cls: Self, data: dict[str, str]) -> Self:
        """Return a MqttMsgPayload instance from a dictionary containing a topic and message."""

        return MQTTMessagePayload(topic=data["topic"], message=data["message"])


@dataclass
class MQTTMessageResult:
    """A data wrapper for the results of MQTT messages."""

    published: bool
    message_id: str


def connect_client(
    config: MQTTClientConfig, tls_ca_cert_path: Path, keepalive: int = 60
) -> MQTTClient:
    """Return a MQTT client using the input config and path to a valid TLS CA certificate."""

    def on_connect(
        client: MQTTClient,
        userdata: Any,
        connect_flags: MQTTConnectFlags,
        reason_code: MQTTReasonCode,
        properties: MQTTProperties,
    ):
        # 0 == success
        if reason_code == 0:
            logger.info("CONNECTION SUCCESS: Connected to MQTT broker.")


    def on_connect_fail(
        client: MQTTClient,
        userdata: Any,
        connect_flags: MQTTConnectFlags,
        reason_code: MQTTReasonCode,
        properties: MQTTProperties,
    ):
        logger.info("CONNECTION FAILURE: Did not connect to MQTT broker.")

    # Modern Paho MQTT client objects require a specified callback_api_version
    client = MQTTClient(
        client_id=config.client_id,
        callback_api_version=MQTTCallbackAPIVersion.VERSION2,
    )

    # Bootstrap the client with certs, credentials, and handlers
    client.tls_set(ca_certs=str(tls_ca_cert_path))
    client.username_pw_set(username=config.username, password=config.password)
    client.on_connect_fail = on_connect_fail
    client.on_connect = on_connect

    client.connect(host=config.broker, port=config.port, keepalive=keepalive)

    logger.info("Returning MQTT client.")
    return client


def handle_publish_result(msg_info: MQTTMessageInfo) -> MQTTMessageResult:
    """Sort through the data returned by the MQTT broker and return a managable result."""

    return MQTTMessageResult(published=msg_info.is_published(), message_id=msg_info.mid)


def publish_message(
    client: MQTTClient, publish_payload: MQTTMessagePayload
) -> MQTTMessageResult | None:
    """Publish a message to the configured topic."""

    try:
        return handle_publish_result(
            client.publish(topic=publish_payload.topic, payload=publish_payload.message)
        )
    except ValueError as err:
        logger.error(
            f"Problem with payload for message '{publish_payload.message}' to topic '{publish_payload.topic}'"
        )
        logger.error(err)
        raise (err)


def subscribe(client: MQTTClient, topic: str) -> None:
    """Subscribe to messages published to a specific topic."""

    def on_message(client: MQTTClient, userdata: Any, msg: MQTTMessage):
        logger.info(f"Received `{msg.payload.decode()}` from topic `{msg.topic}`")

    logger.info(f"Subscribing to topic {topic}")
    client.subscribe(topic=topic)
    client.on_message = on_message
