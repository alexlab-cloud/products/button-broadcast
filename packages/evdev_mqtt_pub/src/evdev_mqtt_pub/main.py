from pathlib import Path
import evdev

from loguru import logger

import mqtt_utils.core as mqtt


def publish_msg(topic: str, msg: str, tls_ca_cert_path: Path) -> None:
    client_config = mqtt.MQTTClientConfig.from_env()
    client = mqtt.connect_client(
        config=client_config, tls_ca_cert_path=tls_ca_cert_path
    )

    client.loop_start()
    payload = mqtt.MQTTMessagePayload(topic=topic, message=msg)
    logger.info(f"Publishing message `{msg}` to topic `{topic}`.")

    result = mqtt.publish_message(client=client, publish_payload=payload)
    logger.info(f"message_id: {result.message_id}")
    client.loop_stop()


# if __name__ == "__main__":
#     publish_msg(
#         topic="button-msg",
#         msg="Just one?",
#         tls_ca_cert_path="../../certs/emqxsl-ca.crt",
#     )

if __name__ == "__main__":
    devices = [evdev.InputDevice(path) for path in evdev.list_devices("/dev/input")]
    for device in devices:
        print(f"{device.path} :: {device.name} :: {device.phys}")
