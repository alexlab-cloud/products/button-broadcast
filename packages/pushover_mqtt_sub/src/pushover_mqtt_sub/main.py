from pathlib import Path

from loguru import logger

import mqtt_utils.core as mqtt


def subscribe_and_run(topic: str, msg: str, tls_ca_cert_path: Path) -> None:
    client_config = mqtt.MQTTClientConfig.from_env()
    client_config.client_id = "different-100"
    client = mqtt.connect_client(
        config=client_config, tls_ca_cert_path=tls_ca_cert_path
    )

    mqtt.subscribe(client=client, topic=topic)
    logger.info("Starting subscription loop.")
    client.loop_forever()


if __name__ == "__main__":
    subscribe_and_run(
        topic="button-msg",
        msg="Just one?",
        tls_ca_cert_path="../../certs/emqxsl-ca.crt",
    )
